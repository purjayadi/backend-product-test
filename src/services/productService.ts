import HttpException from '../middleware/HttpException';
import ProductRepository from '../database/repository/productRepository';
import { getAllDataFilters } from '../dto';
import { ProductInput } from '../interfaces';
import { paginate as pagination } from './../utils/paginate';

class ProductService {
    repository: ProductRepository;

    constructor() {
        this.repository = new ProductRepository();
    }

    async getAllProduct(filters: getAllDataFilters) {
        try {
            const product = await this.repository.products(filters);
            const productPaginate = pagination(product, filters?.page, filters?.limit);
            return productPaginate;
        } catch (error: any) {
            throw new Error(error);
        }
    }

    async createProduct(payload: ProductInput) {
        try {
            const data = {
                ...payload,
                picture: payload.picture?.replace(/^data:image\/[a-z]+;base64,/, '')
            };
            const product = await this.repository.create(data);
            return product;   
        } catch (error: any) {
            throw new Error(error);
        }
    }

    async updateProduct(id: string | number, payload: ProductInput) {
        try {
            const product = await this.repository.productByID(id);
            if (!product) {
                throw new HttpException(404, 'Product Not found');
            }
            const picture = payload.picture ? payload.picture.replace(/^data:image\/[a-z]+;base64,/, '') : product.picture;
            const data = {
                ...payload,
                picture
            };
            const updateProduct = await this.repository.update(id, data);
            return updateProduct;
        } catch (error: any) {
            throw new HttpException(500, error);
        }
    }

    async deleteProduct(id: string | number) {
        try {
            const product = await this.repository.productByID(id);
            if (!product) {
                throw new HttpException(404, 'Product Not found');
            }
            const deleteProduct =  this.repository.delete(id);
            return deleteProduct;
        } catch (error: any) {
            throw new HttpException(404, error.message);
        }
    }

    async getProductById(id: string | number) {
        const product = await this.repository.productByID(id);
        return product;
    }
}

export default ProductService;