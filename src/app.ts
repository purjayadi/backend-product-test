import express from 'express';
import morganMiddleware from './middleware/morganMiddleware';
import productRoute from './route';
import cors from 'cors';
import errorMiddleware from './middleware/errorMiddleware';

const app = express();

app.use(cors());
app.use(express.json({
    type: '*/*'
}));
app.use(express.urlencoded({
    extended: true
}));

app.use(morganMiddleware);
app.use('/api/v1', productRoute);
app.use(errorMiddleware);

export default app;