import { param, query, body } from 'express-validator';

class ProductValidator {
	checkReadProduct() {
		return [
			query('page')
				.optional()
				.isNumeric()
				.withMessage('The value should be number'),
			query('limit')
				.optional()
				.isNumeric()
				.withMessage('The value should be number'),
			query('order')
				.optional()
				.isIn(['ASC', 'DESC'])
				.withMessage('The value should be ASC or DESC'),
		];
	}

	checkIdParam() {
		return [
			param('id')
				.notEmpty()
				.withMessage('The value should be not empty')
		];
	}

	checkCreateProduct() {
		return [
			body('name')
				.notEmpty()
				.withMessage('Name is required'),
			body('qty')
				.notEmpty()
				.isNumeric()
				.withMessage('Qty is required'),
			body('picture')
				.notEmpty()
				.withMessage('Picture is required'),
			body('expiredAt')
				.notEmpty()
				.withMessage('ExpiredAt is required'),
		];
	}

	checkUpdateProduct() {
		return [
			param('id')
				.notEmpty()
				.withMessage('The value should be not empty'),
			body('name')
				.notEmpty()
				.withMessage('Name is required'),
			body('qty')
				.notEmpty()
				.withMessage('Qty is required'),
			body('expiredAt')
				.notEmpty()
				.withMessage('ExpiredAt is required'),
			body('isActive')
				.isEmpty()
				.withMessage('Dont put isActive at body'),
		];
	}
}

export default new ProductValidator();