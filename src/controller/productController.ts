import { Request, Response } from 'express';
import { ProductService } from '../services';
import { getAllDataFilters } from '../dto';
import Logger from '../utils/logger';

const service = new ProductService();
class ProductController {

    async getAllProduct(req: Request, res: Response) {
        try {
            const params: getAllDataFilters =  req.query;
            const data = await service.getAllProduct(params);
            return res.status(200).json({
                success: true,
                data: data
            });
        }
        catch (error: any) {
            return res.status(500).json({
                success: false,
                message: error.message
            });
        }
    }

    async getProductById(req: Request, res: Response) {
        try {
            const id = req.params.id;
            const data = await service.getProductById(id);

            Logger.debug(data);
            return res.status(200).json({
                success: true,
                data
            });
        }
        catch (error: any) {
            return res.status(error.status).json({
                success: false,
                message: error.message
            });
        }
    }

    async createProduct(req: Request, res: Response) {
        try {
            const payload = req.body;
            const data = await service.createProduct(payload);
            return res.status(200).json({
                success: true,
                data
            });
        }
        catch (error: any) {
            return res.status(500).json({
                success: false,
                message: error.message
            });
        }
    }

    async updateProduct(req: Request, res: Response) {
        try {
            const id = req.params.id;
            const payload = req.body;
            const data = await service.updateProduct(id, payload);
            return res.status(200).json({
                success: true,
                data
            });
        }
        catch (error: any) {
            return res.status(500).json({
                success: false,
                message: error.message
            });
        }
    }

    async deleteProduct(req: Request, res: Response) {
        try {
            const id = req.params.id;
            await service.deleteProduct(id);
            return res.status(200).json({
                success: true,
                message: 'Product deleted successfully'
            });
        }
        catch (error: any) {
            console.log(error);
            
            return res.status(error.status).json({
                success: false,
                message: error.message
            });
        }
    }

}

export default ProductController;