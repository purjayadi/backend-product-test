export class getAllDataFilters {
    public readonly limit?: number;
    public readonly page?:number;
    public readonly sort?: string;
    public readonly order?: string;
    public readonly search?: string;
}
