import { IProduct } from './../interfaces/index';

export class paginate {
    public readonly rows!: IProduct[];
    public readonly count!: number;
}
