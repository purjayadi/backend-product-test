import express from 'express';
import middleware from '../middleware/handleValidationMiddleware';
import { ProductController } from '../controller';
import ProductValidator from '../validator';

const router = express.Router();
const productController = new ProductController();

router.get(
	'/product',
	ProductValidator.checkReadProduct(),
    middleware.handleValidationError,
	productController.getAllProduct
);

router.get(
    '/product/:id',
    ProductValidator.checkIdParam(),
    productController.getProductById
);

router.post(
    '/product',
    ProductValidator.checkCreateProduct(),
    middleware.handleValidationError,
    productController.createProduct
);

router.put(
    '/product/:id',
    ProductValidator.checkUpdateProduct(),
    middleware.handleValidationError,
    productController.updateProduct
);

router.delete(
    '/product/:id',
    ProductValidator.checkIdParam(),
    middleware.handleValidationError,
    productController.deleteProduct
);

export default router;