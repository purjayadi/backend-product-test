import { Optional } from 'sequelize/types';

export interface IProduct{
    id?: string;
    name: string;
    qty: number;
    picture: string | undefined;
    expiredAt: Date;
    isActive?: boolean
}

export interface ProductInput extends Optional<IProduct, 'id'> {}
export interface ProductOutput extends Required<IProduct> {}