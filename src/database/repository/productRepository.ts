const { Op } = require('sequelize');
import HttpException from '../../middleware/HttpException';
import { getAllDataFilters, paginate } from '../../dto';
import { ProductInput, ProductOutput } from '../../interfaces';
import Product from '../models/product';

class ProductRepository {

    async products(filters?: getAllDataFilters
    ): Promise<paginate> {
        const limit = filters?.limit ? +filters?.limit : 10;
        const offset = filters?.page ? (+filters?.page * limit) - limit : 1;
        const order = filters?.order ? filters?.order : 'DESC';
        const sort = filters?.sort ? filters?.sort : 'createdAt';
        const allProduct = Product.findAndCountAll({
            ...filters?.page && { offset: offset },
            ...filters?.limit && { limit: limit },
            ...filters?.sort && {
                order: [
                    [sort, order]
                ]
            },
            ...filters?.search && {
                where: { 
                    [Op.or]: [
                        { name: { [Op.like]: `%${filters?.search}%` } }
                    ]
                }
            }
        });
        return allProduct;
    }

    async create(payload: ProductInput): Promise<ProductOutput> {
        try {
            const product = await Product.create(payload);
            return product;
        } catch (error: any) {
            throw new HttpException(500, error);
        }
    }

    async update(id: string | number, payload: Partial<ProductInput>) {
        try {
            const updateProduct = await Product.update(payload, {
                where: { id },
            });
            return updateProduct;
        } catch (error: any) {
            throw new HttpException(500, error);
        }
    }

    async delete(id: string | number): Promise<boolean> {
        const product = await Product.destroy({
            where: { id },
            individualHooks: true
        });
        return !!product;
    }

    async productByID(id: string | number): Promise<ProductOutput> {
        const product = await Product.findByPk(id);
        if (!product) {
            throw new HttpException(404, 'Product not found');
        }
        return product;
    }
}

export default ProductRepository;