'use strict';
const { faker } = require('@faker-js/faker');
const imageToBase64 = require('image-to-base64');

module.exports = {
  async up(queryInterface) {
    let product = [];
    for (let i = 0; i < 30; i++) {
      product.push({
        name: faker.commerce.productName(),
        qty: faker.random.numeric(3),
        picture: await imageToBase64(faker.image.food()),
        expiredAt: faker.date.future(),
        isActive: true,
        createdAt: new Date(),
        updatedAt: new Date(),
        deletedAt: null,
      });
    }
    await queryInterface.bulkInsert('Products', product);
  },

  async down() {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
