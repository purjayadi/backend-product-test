'use strict';
import { DataTypes, Model } from 'sequelize';
import db from '../../config/db';
import { IProduct, ProductInput } from '../../interfaces';

class Product
    extends Model<IProduct, ProductInput>
    implements IProduct {
    declare id: string;
    public name!: string;
    public qty!: number;
    public picture!: string;
    public expiredAt!: Date;
    public isActive!: boolean;

    // timestamps!
    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
    public readonly deletedAt!: Date;
}

Product.init(
    {
        id: {
            type: DataTypes.BIGINT,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false,
            unique: true,
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        qty: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        picture: {
            type: DataTypes.TEXT,
            allowNull: false,
        },
        expiredAt: {
            type: DataTypes.DATEONLY,
            allowNull: false,
        },
        isActive: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: true,
        }
    },
    {
        sequelize: db,
        timestamps: true,
        paranoid: true,
    }
);

// after destroy product set isActive to false
Product.beforeDestroy(async (product) => {
    product.isActive = false;
    await product.save();
});

// block direct set isActive from form
// Product.beforeUpdate((product, options) => {
//     Logger.info(options);
//     if (options.fields?.includes('isActive')) {
//         throw new Error('Cannot update isActive directly');
//     }
// });

export default Product;