# Backend Product

## Getting Started

First, run the docker compose:

```bash
docker compose up
docker exec -it apiProduct bash
```

Install package and run the development server: using [`yarn`](https://yarnpkg.com):

```bash
yarn install
yarn sequelize db:migrate
yarn sequelize db:seed:all
yarn dev
```

Or [`npm`](https://www.npmjs.com):

```bash
npm install
npm run sequelize db:migrate
npm run sequelize db:seed:all
npm run dev
```

Endpoint proxy nginx [http://localhost:3000](http://localhost:3000).
