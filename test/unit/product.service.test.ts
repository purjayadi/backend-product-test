import { StatusCodes } from 'http-status-codes';
import db from '../../src/config/db';
import Product from '../../src/database/models/product';
import { ProductService } from '../../src/services';

describe('Test product service', () => {
	const product = {
		name: 'test 2',
		qty: 100,
		picture: 'test',
		expiredAt: new Date(),
		isActive: true
	};

	afterAll(async () => {
		await db.close();
	});

	let service: any;
	beforeEach(() => {
		service = new ProductService();
	});

	it('should return all product', async () => {
		let params = {
			page: 1,
			offset: 10
		};
		const data = await service.getAllProduct(params);
		expect(StatusCodes.OK);
		expect(data).toHaveProperty('items');
		expect(data).toHaveProperty('totalItems');
	});

	it('should return product by id', async () => {
		const mockGetProduct = jest.fn((): any => (product));
		jest
			.spyOn(Product, 'findByPk')
			.mockImplementation(() => mockGetProduct());
		const data = await service.getProductById('546462128');
		expect(data).toHaveProperty('name');  // Success!
	});

	// it should be create product
	it('should be create product', async () => {
		const mockCreateProduct = jest.fn((): any => product);
		jest
			.spyOn(Product, 'create')
			.mockImplementation(() => mockCreateProduct());
		const data = await service.createProduct(product);
		expect(data).toHaveProperty('name');  // Success!
	});

	// it should update product
	it('should update product', async () => {
		const mockUpdateProduct = jest.fn((): any => product);
		jest
			.spyOn(Product, 'update')
			.mockImplementation(() => mockUpdateProduct());
		const data = await service.updateProduct('546462128', product);
		expect(data).toHaveProperty('name');  // Success!
	});

	// it should be delete product
	it('should be delete product', async () => {
		const mockDeleteProduct = jest.fn((): any => true);
		jest
			.spyOn(Product, 'destroy')
			.mockImplementation(() => mockDeleteProduct());
		const data = await service.deleteProduct('128');
		expect(data).toBe(true);
	});

});
