import { StatusCodes } from 'http-status-codes';
import request from 'supertest';
import app from '../../src/app';
import db from '../../src/config/db';
import Product from '../../src/database/models/product';
import { IProduct } from '../../src/interfaces';

let product: IProduct = {
    name: 'test 2',
    qty: 100,
    picture: 'test',
    expiredAt: new Date(),
    isActive: true
};
describe('Test create product route', () => {
    it('Should return success created product', async () => {
        const mockCreateProduct = jest.fn((): any => product);
		jest
			.spyOn(Product, 'create')
			.mockImplementation(() => mockCreateProduct());
		const res = await request(app).post('/api/v1/product').send(product);
        expect(StatusCodes.OK);
		expect(res.body).toHaveProperty('data');
		expect(res.body.success).toBe(true);
    });
});

describe('Test update product route', () => {

    it('Should handle exception when put isActive to body', async () => {
        const mockUpdateProduct = jest.fn((): any => product);
        jest
            .spyOn(Product, 'update')
            .mockImplementation(() => mockUpdateProduct());
        const res = await request(app).put('/api/v1/product/182').send(product);
        expect(StatusCodes.BAD_REQUEST);
        expect(res.body).toEqual({
			'value': true,
            'msg': 'Dont put isActive at body',
            'param': 'isActive',
            'location': 'body'
		});
    });

    // it should be update product
    it('Should return success updated product', async () => {
        const mockUpdateProduct = jest.fn((): any => product);
        jest
            .spyOn(Product, 'update')
            .mockImplementation(() => mockUpdateProduct());
        await request(app).put('/api/v1/product/182').send(product);
        expect(StatusCodes.OK);
    });

});

describe('Test get product route', () => {    
    it('Should return all product', async () => {
        const mockGetProduct = jest.fn((): any => product);
        jest
            .spyOn(Product, 'findAndCountAll')
            .mockImplementation(() => mockGetProduct());
        const res = await request(app).get('/api/v1/product');
        expect(res.body).toHaveProperty('data');
        expect(res.body.success).toBe(true);
        expect(StatusCodes.OK);
    });

    it('Should return product by id', async () => {
        const mockGetProduct = jest.fn((): any => product);
        jest
            .spyOn(Product, 'findByPk')
            .mockImplementation(() => mockGetProduct());
        const res = await request(app).get('/api/v1/product/182');
        expect(res.body).toHaveProperty('data');
        expect(res.body.success).toBe(true);
        expect(StatusCodes.OK);
    });

    afterAll(async () => {
        await db.close();
    });

});
